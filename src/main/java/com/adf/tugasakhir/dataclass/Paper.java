package com.adf.tugasakhir.dataclass;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "paper")
@Data
public class Paper {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "title")
    private String title;

    @NotNull
    @Column(name = "abstrak")
    private String abstrak;

    @NotNull
    @Column(name = "url")
    private String url;


    public Paper(String title, String url, String abstrak) {
        this.title = title;
        this.abstrak = abstrak;
        this.url = url;
    }
}

